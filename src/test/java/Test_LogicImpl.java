import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import parser_products.config.ConfigApplication;
import parser_products.model.Good;
import parser_products.processing.logic.LogicImpl;

public class Test_LogicImpl {

	private static List<Good> testData = new ArrayList<>();

	private static LogicImpl logic;

	private static List<Good> expected = new ArrayList<>();

	@BeforeClass
	public static void loadProperties() {

		ConfigApplication.loadProperties("test.properties");
		for (int j = 1; j < 4; j++)
			for (int i = 3; i > 0; i--) {
				{

					expected.add(createGood(i, 1.0F * j));

				}
			}
		
		
	}

	@Before
	public void setUp() throws Exception {

		logic = new LogicImpl();

		// create 1 group

		for (int i = 1; i < 5; i++) {

			testData.add(createGood(1, 1.0F * i));

		}

		// create 2 group

		for (int i = 1; i < 8; i++) {

			testData.add(createGood(2, 1.0F * i));

		}

		// create 3 group

		for (int i = 1; i < 4; i++) {

			testData.add(createGood(3, 1.0F * i));

		}

	}

	private static Good createGood(int id, float price) {

		Good good = new Good();

		good.setId(id);

		good.setName("Name" + id);

		good.setCondition("Condition" + id);

		good.setState("State" + id);

		good.setPrice(price);

		return good;

	}

	@Test
	public void testLogic() {

		List<Good> actual = logic.apply(testData);

		//actual.forEach(x -> System.out.println(x.getId() + "," + x.getPrice()));

		// expected.forEach(x->System.out.println(x.getId()+","+x.getPrice()));

		Assert.assertArrayEquals(expected.toArray(), actual.toArray());

	}

}
