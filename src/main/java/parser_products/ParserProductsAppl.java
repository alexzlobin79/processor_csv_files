package parser_products;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parser_products.config.ConfigApplication;
import parser_products.model.Good;
import parser_products.parser.ParserFileImpl;
import parser_products.processing.IProcessingFiles;
import parser_products.processing.ProcessingServiceImpl;
import parser_products.processing.logic.LogicImpl;
import parser_products.writer.IWriterData;
import parser_products.writer.WriterGoodsImpl;

/**
 * <h5>Application for processing files</h5>
 * 
 * 
 * @author Zlobin Alex(avzlobin79@gmail.com)
 * @version 0.0.1
 */

public class ParserProductsAppl {

	private static final Logger logger = LoggerFactory.getLogger(ParserProductsAppl.class);

	private static final String FILE_PROPERTIES = "application.properties";

	/**
	 * This is the main method which run processing csv file with goods.
	 * 
	 * 
	 */

	public static void main(String[] args) {

		try {

			ConfigApplication.loadProperties(FILE_PROPERTIES);

			long t1 = System.currentTimeMillis();

			logger.info("start processing files , {}", LocalDate.now());

			IProcessingFiles<Good> processor = new ProcessingServiceImpl(new LogicImpl(),
					new ParserFileImpl());

			Iterable<Good> result=processor.runAndGetResult();
			

			logger.info("end processing files , {} , time working {} ms , isExistError {}", LocalDate.now(),
					System.currentTimeMillis() - t1, processor.isExistError());
			
			
			IWriterData<Good> writer=new WriterGoodsImpl();
			
			writer.write(result);
			
			logger.info("the result was successfully written to the file, {}", LocalDate.now());
			
			System.exit(0);
			

		} catch (Exception ex) {

			logger.error("error!", ex);
			

			System.exit(-1);

		}

	}

}
