package parser_products.processing;

import java.util.List;

import parser_products.model.Good;

public interface IProcessingFiles<T> {

	List<Good> runAndGetResult();
	
	boolean isExistError();

}
