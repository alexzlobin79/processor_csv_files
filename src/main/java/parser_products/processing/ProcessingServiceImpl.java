package parser_products.processing;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parser_products.config.ConfigApplication;
import parser_products.exception.ParserFileException;
import parser_products.exception.ProccessingFileException;
import parser_products.exception.ProcessingFileException;
import parser_products.model.Good;
import parser_products.parser.IParserFile;
import parser_products.processing.logic.ILogicProcessing;

public class ProcessingServiceImpl implements IProcessingFiles<Good> {

	private final ILogicProcessing<Good> logic;

	private final Executor executor;

	private final String directoryProcessing;
    
	
	private final IParserFile<Good> parser;
	
	private  boolean existError=false;

	private static final Logger logger = LoggerFactory.getLogger(ProcessingServiceImpl.class);
	
	public ProcessingServiceImpl( ILogicProcessing<Good> logic,IParserFile<Good> parser) {

		
		this.directoryProcessing = ConfigApplication.getBasePath()
				+ ConfigApplication.getDirProcessing();
		this.logic = logic;
		this.executor = Executors.newFixedThreadPool(ConfigApplication.getNumThreads());
		this.parser=parser;

	}

	private CompletableFuture<List<Good>> startWorker(String relativePathFile) {

		return CompletableFuture.supplyAsync(new Supplier<List<Good>>() {

			@Override
			public List<Good> get() {
				// we will not catch error if we give always result for any cause, see
				// "exceptionally"?
				try {

					List<Good> result = logic
							.apply(parser.copy().getResult(directoryProcessing + "/" + relativePathFile));

					logger.info("file has processed: {}",relativePathFile);

					return result;

				} catch (ParserFileException e) {

					throw new ProcessingFileException("error parser file: " + relativePathFile, e);
				}

			}

		}, executor).exceptionally(throwable -> {

			logger.error("error processing file: {}" , relativePathFile, throwable);
			// return empty result !!!
			//we must to write error from logs
			existError=true;
			
			return Collections.emptyList();

		});

	}

	
	
	public boolean isExistError() {
		return existError;
	}

	@Override
	public List<Good> runAndGetResult() {

		String[] files = getPathFilesFromDirectory(directoryProcessing);

		if (files.length == 0)
			return Collections.emptyList();

		// Run processing parallel and combine result

		List<Good> combineResult = Arrays.stream(files).map(x -> startWorker(x)).map(CompletableFuture::join)
				.flatMap(x -> x.stream()).collect(Collectors.toList());

		// And again processing last time

		return logic.apply(combineResult);

	}

	private String[] getPathFilesFromDirectory(String path) {

		File dir = new File(path);

		if (!(dir.exists() && dir.isDirectory()))
			throw new ProccessingFileException(
					"path for processing files of goods is not exist or is not directory: " + path);

		String[] paths = dir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(ConfigApplication.getFileExtension());
			}
		});

		return paths;

	}
}
