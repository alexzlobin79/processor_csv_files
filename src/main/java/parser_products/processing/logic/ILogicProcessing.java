package parser_products.processing.logic;

import java.util.List;

public interface ILogicProcessing<T> {
    
	List<T> apply(List<T> data);
}
