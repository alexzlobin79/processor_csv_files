package parser_products.processing.logic;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import parser_products.config.ConfigApplication;
import parser_products.model.Good;

public class LogicImpl implements ILogicProcessing<Good> {

	private final int maxCountPositionEqualId;

	private final int maxCountPosition;

	public LogicImpl() {

		this.maxCountPositionEqualId = ConfigApplication.getMaxCountPositionEqualId();
		this.maxCountPosition = ConfigApplication.getMaxCountPosition();
	}

	@Override
	public List<Good> apply(List<Good> data) {

		List<Good> list = data;

		// no meaning in processing
		if (data.size() < 2)

			return list;

		// Sort desc id with grouping by price(asc)
		list.sort(new ComparatorByIdAndPrice());

		// remove elements that are repeated more than N times

		// counter duplicate elements
		int count = 0;

		// we need to use only iterator for removing
		// starting with 2 elements
		ListIterator<Good> it = list.listIterator(1);

		// for fast deleting elements we need to use LinkedList with iterator!

		// set starting value

		Good prev = list.get(0);

		Good current = null;

		while (it.hasNext()) {

			current = it.next();

			if (!prev.getId().equals(current.getId())) {

				count = 0;
			} else if (count + 1 < maxCountPositionEqualId) {

				count++;

			} else {

				it.remove();

			}

			prev = current;

		}
		// again sorting data only price and take only limit positions
		List<Good> result = list.parallelStream().sorted((x, y) -> Float.compare(x.getPrice(), y.getPrice()))
				.limit(maxCountPosition).collect(Collectors.toList());

		return result;

	}

	private class ComparatorByIdAndPrice implements Comparator<Good> {

		public ComparatorByIdAndPrice() {
			// TODO Auto-generated constructor stub
		}

		public int compare(Good x, Good y) {

			int compareId = Integer.compare(y.getId(), x.getId());

			if (compareId != 0)
				return compareId;

			return Float.compare(x.getPrice(), y.getPrice());

		}

	}

}
