package parser_products.config;

class ConfigProperty {

	public static final String HEADERS = "parser.headers";

	public static final String LINE_SEPARATOR = "parser.line_separator";

	public static final String HEADER_EXTRACTION_ENABLED = "parser.header_extraction_enabled";

	public static final String DELIMETER = "parser.delimeter";

	public static final String CHARSET_NAME = "parser.charset_name";

	public static final String FILE_EXTENSION = "processing.file_extension";

	public static final String NUMBER_THREADS = "processing.number_threads";

	public static final String DIRECTORY_PROCESS_FILE = "processing.directory";

	public static final String MAX_COUNT_POSITION_EQUAL_ID = "processing.max_count_position_equal_id";

	public static final String MAX_COUNT_POSITION = "processing.max_count_position";
	
	public static final String BASE_DIRECTORY = "base_directory";
	
	public static final String RESULT_DIRECTORY = "result.directory";

}
