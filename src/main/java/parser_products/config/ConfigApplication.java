package parser_products.config;

import java.io.File;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import parser_products.exception.LoadingConfigException;
import parser_products.exception.PropertyConfigException;

//ConfigApplication is threadsafe see:
//https://commons.apache.org/proper/commons-configuration/userguide/howto_concurrency.html

public class ConfigApplication {

	private final static Configurations configs = new Configurations();
	
	private static Configuration config;

	public static  void loadProperties(final String fileProperties) {

		// reading file properties

		try {

			config = configs.properties(new File(fileProperties));

		} catch (ConfigurationException e) {

			throw new LoadingConfigException("Error load file properties: " + fileProperties, e);

		}

	}

	public static String getBasePath() {
		// by default
		if (!containsProperty(ConfigProperty.BASE_DIRECTORY))
			return (new File("").getAbsolutePath());
		
		return config.getString(ConfigProperty.BASE_DIRECTORY);

	}

	public static String[] getHeaders() {

		return getPropertyStringArray(ConfigProperty.HEADERS);

	}

	public static  String getLineSeparator() {

		return getPropertyString(ConfigProperty.LINE_SEPARATOR);

	}

	public static boolean getHeaderExtractionEnabled() {

		return getPropertyBoolean(ConfigProperty.HEADER_EXTRACTION_ENABLED);

	}

	public static char getDelimeter() {

		return getPropertyChar(ConfigProperty.DELIMETER);

	}

	public static String getCharsetName() {

		return getPropertyString(ConfigProperty.CHARSET_NAME);

	}

	public static String getFileExtension() {

		return getPropertyString(ConfigProperty.FILE_EXTENSION);

	}

	public static int getNumThreads() {

		return getPropertyInt(ConfigProperty.NUMBER_THREADS);

	}

	public static String getDirProcessing() {

		return getPropertyString(ConfigProperty.DIRECTORY_PROCESS_FILE);

	}

	public static int getMaxCountPositionEqualId() {

		return getPropertyInt(ConfigProperty.MAX_COUNT_POSITION_EQUAL_ID);

	}

	public static int getMaxCountPosition() {

		return getPropertyInt(ConfigProperty.MAX_COUNT_POSITION);

	}
	
	public static String getDirResult() {

		return getPropertyString(ConfigProperty.RESULT_DIRECTORY);

	}

	private static boolean getPropertyBoolean(final String property) {

		checkProperty(property);

		return config.getBoolean(property);

	}

	private static char getPropertyChar(final String property) {

		checkProperty(property);

		return config.getString(property).charAt(0);

	}

	private static String[] getPropertyStringArray(final String property) {

		checkProperty(property);

		String[] result = config.getStringArray(property);

		if (result.length == 0)
			throw new PropertyConfigException("array is empty: " + property);

		return config.getStringArray(property);

	}

	private static String getPropertyString(final String property) {

		checkProperty(property);

		String result = config.getString(property);

		if (StringUtils.isEmpty(result))

			throw new PropertyConfigException("empty property: " + property);

		return result;

	}

	private static int getPropertyInt(final String property) {

		checkProperty(property);

		return config.getInt(property);
	}

	private static boolean containsProperty(final String property) {

		return config.containsKey(property);

	}

	private static void checkProperty(final String property) {

		if (!containsProperty(property))
			throw new PropertyConfigException("no property: " + property);

	}

}
