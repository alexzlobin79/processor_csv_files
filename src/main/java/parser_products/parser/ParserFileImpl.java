package parser_products.parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import parser_products.config.ConfigApplication;
import parser_products.exception.ParserFileException;
import parser_products.model.Good;

public class ParserFileImpl implements IParserFile<Good>, Cloneable {

	private final CsvParserSettings settings = new CsvParserSettings();

	private final CsvParser parser;

	public ParserFileImpl() {

		initSettings();

		parser = new CsvParser(settings);

	}

	private ParserFileImpl(CsvParser parser) {

		this.parser = parser;

	}

	private void initSettings() {

		settings.setHeaders(ConfigApplication.getHeaders());
		settings.getFormat().setLineSeparator(ConfigApplication.getLineSeparator());
		settings.setHeaderExtractionEnabled(ConfigApplication.getHeaderExtractionEnabled());
		settings.setDelimiterDetectionEnabled(true, ConfigApplication.getDelimeter());

	}

	private Reader getReader(String absolutePath) throws UnsupportedEncodingException, FileNotFoundException {

		InputStream in = new FileInputStream(absolutePath);

		return new InputStreamReader(in, ConfigApplication.getCharsetName());

	}

	@Override
	public ParserFileImpl copy() {
		return new ParserFileImpl(this.parser);
	}

	@Override
	public List<Good> getResult(String path) throws ParserFileException {

		// after getting we have to process data (removing,sort data). LinkedList allows
		// removing using iterator very fast

		List<Good> result = new LinkedList<>();

		// call beginParsing to read records one by one, iterator-style.
		try {
			parser.beginParsing(getReader(path));

		} catch (UnsupportedEncodingException e) {

			throw new ParserFileException("unsupported encoding", e);

		} catch (FileNotFoundException e) {

			throw new ParserFileException("file for parsing not found", e);

		}

		Record record;
		while ((record = parser.parseNextRecord()) != null) {

			Good good = new Good();

			good.setId(record.getInt("ID"));
			good.setName(record.getString("Name"));
			good.setCondition(record.getString("Condition"));
			good.setState(record.getString("State"));
			good.setPrice(record.getFloat("Price"));

			result.add(good);

		}

		parser.stopParsing();
		return result;
	}

}
