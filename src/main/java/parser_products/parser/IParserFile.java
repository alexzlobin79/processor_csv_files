package parser_products.parser;
import java.util.List;

import parser_products.exception.ParserFileException;


public interface IParserFile<T>  {

	List<T> getResult(String path) throws ParserFileException;
	//for working parallel
	IParserFile<T> copy();
    
	
}