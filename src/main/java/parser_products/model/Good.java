package parser_products.model;

//columns:product ID (integer), Name (string), Condition (string),State (string),Price (float)
public class Good {

	private Integer id;

	private String name;

	private String condition;

	private String state;

	private float price;

	public Good(Integer id, String name, String condition, String state, float price) {
		super();
		this.id = id;
		this.name = name;
		this.condition = condition;
		this.state = state;
		this.price = price;
	}

	public Good() {

	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCondition() {
		return condition;
	}

	public String getState() {
		return state;
	}

	public float getPrice() {
		return price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Good other = (Good) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Good [id=" + id + ", name=" + name + ", condition=" + condition + ", state=" + state + ", price="
				+ price + "]";
	}

}
