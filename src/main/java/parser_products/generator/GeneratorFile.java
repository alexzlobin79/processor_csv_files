package parser_products.generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneratorFile {
	
	private static final String SPLIT = ";";
	private static final int MIN_ID = 100000;
	private static final int MAX_ID = 99999999;
	// price minimum unit of measurement
	private static final int MIN_PRICE = 1000;
	private static final int MAX_PRICE = 10000;
	private static final float MIN_UNIT = 0.01F;
	private static final String LINE_SEPARATOR = "\n";
	// for only creating records:"Name", "Condition", "State"
	private static final int MIN_VALUE = 100;
	private static final int MAX_VALUE = 10000;
	
	private static String[] headers = { "ID", "Name", "Condition", "State", "Price" };
	
	private static final Logger logger = LoggerFactory.getLogger(GeneratorFile.class);
	
	private String nameFile;
	
	private int rows;
	
	
	public GeneratorFile(String nameFile, int rows) {
		super();
		this.nameFile = nameFile;
		this.rows = rows;
	}

	public void writeRandomDataToFile() {

		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(nameFile))) {

			StringBuilder headers_line = new StringBuilder();

			// insert headers

			for (int i = 0; i < headers.length; i++) {

				headers_line.append(headers[i]).append(SPLIT);

			}

			headers_line.append(LINE_SEPARATOR);

			bufferedWriter.write(headers_line.toString());

			for (int j = 0; j < rows; j++) {

				bufferedWriter.write(getRandomRow().concat(LINE_SEPARATOR));

			}
			
		} catch (IOException e) {

			logger.error("error creating csv file", e);
		}

	}

	private String getRandomRow() {

		String[] row = { String.valueOf(getRandomInteger(MIN_ID, MAX_ID)), "Name" + getRandomInteger(MIN_VALUE, MAX_VALUE),
				"Condition" + getRandomInteger(MIN_VALUE, MAX_VALUE), "State" + getRandomInteger(MIN_VALUE, MAX_VALUE),
				String.format(Locale.US,"%.02f", MIN_UNIT * getRandomInteger(MIN_PRICE, MAX_PRICE)) };

		return String.join(SPLIT, row);

	}

	private  int getRandomInteger(int min, int max) {

		Random rand = new Random();
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		return rand.nextInt((max - min) + 1) + min;

	}


}
