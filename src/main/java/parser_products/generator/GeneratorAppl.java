package parser_products.generator;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <h5>Generator Csv file</h5>
 * 
 * @author Zlobin Alex(avzlobin79@gmail.com)
 * @version 0.0.1
 */
public class GeneratorAppl {

	private static final Logger logger = LoggerFactory.getLogger(GeneratorAppl.class);


	private static final String catalogName = "/catalog";

	
	// number file csv
	private static final int N_FILE = 20;
	
	private static final int N_ROWS = 1100;
	
	
	private static final String NAME_FILE = "test.csv";

	private static final int N_THREADS = 100;
	
	private static final String basePath = new File("").getAbsolutePath();
	
	 final static Executor executor = Executors.newFixedThreadPool(N_THREADS);

	public static void main(String[] args) throws IOException {

		generateFiles();
		System.exit(0);

	}

	private static void generateFiles() {

		long start = System.currentTimeMillis();

		CompletableFuture<?>[] futures = new CompletableFuture[N_FILE];
		
		for (int i = 0; i < N_FILE; i++) {

			

			futures[i] = CompletableFuture.runAsync(() -> {

				(new GeneratorFile(basePath + catalogName + "/" + System.nanoTime()  + "_"+NAME_FILE,N_ROWS)).writeRandomDataToFile();

			},executor );
		}

		CompletableFuture.allOf(futures).join();

		logger.info("generator has finished work and created files:{} * {} rows,time working generator(ms):{}", N_FILE,
				N_ROWS, System.currentTimeMillis() - start);

	}

	
}
