package parser_products.writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parser_products.config.ConfigApplication;
import parser_products.exception.WriteResultFileException;
import parser_products.model.Good;

public class WriterGoodsImpl implements IWriterData<Good> {

	private static final Logger logger = LoggerFactory.getLogger(WriterGoodsImpl.class);

	private static final String PREFIX_FILE_RESULT = "result";

	private static final CharSequence DELIMETER_FILE_RESULT = "_";

	public WriterGoodsImpl() {
		
	}

	@Override
	public void write(String file, Iterable<Good> data) {

		try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {

			StringBuilder headers_line = new StringBuilder();

			String[] headers = ConfigApplication.getHeaders();

			// insert headers

			for (int i = 0; i < headers.length; i++) {

				headers_line.append(headers[i]).append(ConfigApplication.getDelimeter());

			}

			headers_line.append(ConfigApplication.getLineSeparator());

			bufferedWriter.write(headers_line.toString());

			Iterator<Good> it = data.iterator();

			while (it.hasNext()) {

				bufferedWriter.write(getRow(it.next()).concat(ConfigApplication.getLineSeparator()));

			}

		} catch (IOException e) {

			logger.error("error record data to file", e);

			throw new WriteResultFileException("error writing result to file", e);

		}

	}
	
	@Override
	public void write(Iterable<Good> data) {

		write(ConfigApplication.getBasePath().concat(ConfigApplication.getDirResult()).concat(generateFileName()),
				data);

	}

	private String generateFileName() {

		String[] partsOfName = { PREFIX_FILE_RESULT, String.valueOf(LocalDate.now().getYear()),
				String.valueOf(LocalDate.now().getMonthValue()),

				String.valueOf(LocalDate.now().getDayOfMonth()), String.valueOf(System.nanoTime()),
				ConfigApplication.getFileExtension() };

		return "/".concat(String.join(DELIMETER_FILE_RESULT, partsOfName));

	}

	private String getRow(Good good) {

		String[] row = { String.valueOf(good.getId()), good.getName(), good.getCondition(), good.getState(),
				String.valueOf(good.getPrice()) };

		return String.join(String.valueOf(ConfigApplication.getDelimeter()), row);

	}

}
