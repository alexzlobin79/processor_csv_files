package parser_products.writer;

public interface IWriterData<T> {

	void write(String file, Iterable<T> data);
	
	void write(Iterable<T> data);

}
