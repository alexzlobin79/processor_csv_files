package parser_products.exception;

@SuppressWarnings("serial")
public class ParserFileException extends Exception {
	public ParserFileException(String message) {
		super(message);
	}

	public ParserFileException(String message, Throwable cause) {
		super(message, cause);
	}
}
