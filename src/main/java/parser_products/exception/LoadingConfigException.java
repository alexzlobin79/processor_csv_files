package parser_products.exception;

@SuppressWarnings("serial")
public class LoadingConfigException extends RuntimeException {
	public LoadingConfigException(String message) {
		super(message);
	}

	public LoadingConfigException(String message, Throwable cause) {
		super(message, cause);
	}
}