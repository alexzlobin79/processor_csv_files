package parser_products.exception;

@SuppressWarnings("serial")
public class PropertyConfigException extends RuntimeException {
	public PropertyConfigException(String message) {
		super(message);
	}

	public PropertyConfigException(String message, Throwable cause) {
		super(message, cause);
	}
}