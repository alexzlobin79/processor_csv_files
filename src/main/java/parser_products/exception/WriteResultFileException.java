

package parser_products.exception;

@SuppressWarnings("serial")
public class WriteResultFileException extends RuntimeException {
	public WriteResultFileException(String message) {
		super(message);
	}

	public WriteResultFileException(String message, Throwable cause) {
		super(message, cause);
	}
}