package parser_products.exception;

@SuppressWarnings("serial")
public class ProccessingFileException extends RuntimeException {
	public ProccessingFileException(String message) {
		super(message);
	}

	public ProccessingFileException(String message, Throwable cause) {
		super(message, cause);
	}
}
