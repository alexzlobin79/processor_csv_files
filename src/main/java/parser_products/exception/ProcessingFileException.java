package parser_products.exception;

@SuppressWarnings("serial")
public class ProcessingFileException extends RuntimeException {
	public ProcessingFileException(String message) {
		super(message);
	}

	public ProcessingFileException(String message, Throwable cause) {
		super(message, cause);
	}
}